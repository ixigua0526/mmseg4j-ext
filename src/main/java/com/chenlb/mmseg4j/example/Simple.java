package com.chenlb.mmseg4j.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;

import com.chenlb.mmseg4j.Dictionary;
import com.chenlb.mmseg4j.MMSeg;
import com.chenlb.mmseg4j.Seg;
import com.chenlb.mmseg4j.SimpleSeg;
import com.chenlb.mmseg4j.Word;

public class Simple {
	String str1 = "桌上lipstick chateau的a4纸上这么a-morir写着3.1 phillip lim";
	String str2 = "我喜欢pa+++3.1 phillip lim级别的护肤品和运动bra以及运动t恤";
	String str3 = "凉席竹席双面hg3323k可折叠碳化mk234yu高档竹凉席1.8m床1.5米席子";
	String str4 = "工信处女干事每月经过下属at&t科室，都要亲口交代24口交换机等技术性器件的安装工作";
	String str5 = "结过婚的和尚未结过婚的人都要到派出所登记";
	String str6 = "在２００８年，我们的英语老师是一个1.8m（180厘米）高的大个子,但是体重只有42.0kg，人长得眉清目秀,婀娜多姿,而且经常身着名牌,比如a happy marilyn,abercrombie & fitch牌子的衣服,而且偶尔穿深v的v领衣服裙子,班里的女生男生都很喜欢她。她总是亲口交代我们,要好好学习。";
	String str7 = "他总是一个劲儿地说个不停";
	String str8 = "研究生命起源 的课题";
	String str9 = "hello my name is dave";
	String str10 = "a'kin 1.8m这是一个canada goose品牌的a'kin鞋子canad goose的adidas/阿迪鞋子a'kin";

	protected Dictionary dic;

	public Simple()
	{
		dic = Dictionary.getInstance();
	}

	protected Seg getSeg()
	{
		return new SimpleSeg(dic);
	}

	public String segWords(Reader input, String wordSpilt) throws IOException
	{
		StringBuilder sb = new StringBuilder();
		Seg seg = getSeg();
		MMSeg mmSeg = new MMSeg(input, seg);
		Word word;
		boolean first = true;
		while ((word = mmSeg.next()) != null){
			//System.out.println(word.getStartOffset()+"|"+word.getLength()+"|"+word.getEndOffset()+"|"+word.getWordOffset()+"|"+word.getString()+"|"+word.getType());

			if (!first)
			{
				sb.append(wordSpilt);
			}
			String w = word.getString();
			sb.append(w);
			first = false;
		}
		return sb.toString();
	}

	public String segWords(String txt, String wordSpilt) throws IOException
	{
		return segWords(new StringReader(txt), wordSpilt);
	}

	private void printlnHelp()
	{
		System.out.println("\n\t-- 说明: 输入 QUIT 或 EXIT 退出");
		System.out.print("\nmmseg4j-" + this.getClass().getSimpleName().toLowerCase() + ">");
	}

	protected void run() throws IOException
	{
		String[] arr = { str1, str2, str3, str4, str5, str6, str7, str8, str9, str10 };
		for (String str : arr)
		{
			System.out.println(segWords(str, "|"));
		}

		printlnHelp();
		String inputStr = null;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while ((inputStr = br.readLine()) != null)
		{
			if (inputStr.equals("QUIT") || inputStr.equals("EXIT"))
			{
				System.exit(0);
			}
			else if ("".equals(inputStr))
			{
				printlnHelp();
			}
			else
			{
				System.out.println(segWords(inputStr, "/"));
				System.out.print("\nmmseg4j-" + this.getClass().getSimpleName().toLowerCase() + ">");
			}
		}
	}


}
